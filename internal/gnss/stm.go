// Copyright 2023 Clayton Craft <clayton@craftyguy.net>
// SPDX-License-Identifier: GPL-3.0-or-later

package gnss

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"
	"sync"
	"syscall"
	"time"

	"github.com/tarm/serial"
	"gitlab.com/postmarketOS/gnss-share/internal/gnss/common"
	"gitlab.com/postmarketOS/gnss-share/internal/nmea"
	"gitlab.com/postmarketOS/gnss-share/internal/pubsub"
)

// Internal pubsub topic, could be made dynamic later if necessary...
const internalTopic = "stm_internal"

type StmDevice struct {
	device     io.ReadWriteCloser
	stopScan   chan struct{}
	openDevice func() (io.ReadWriteCloser, error)
	ready      func(time.Duration) error
	bus        *pubsub.PubSub
	path       string
	ref        common.Reference
	mu         sync.Mutex
	debug      bool
}

func NewStmSerial(path string, baud int, debug bool) *StmDevice {
	s := StmDevice{
		bus:      pubsub.New(pubsub.DefaultBufferSize),
		debug:    debug,
		path:     path,
		stopScan: nil,
	}

	s.openDevice = func() (io.ReadWriteCloser, error) {
		return serial.OpenPort(&serial.Config{
			Name: path,
			Baud: baud,
		})
	}
	s.ready = func(_ time.Duration) error {
		// Send the command to resume just in case the module is in a suspended
		// state
		if s.debug {
			fmt.Println("stm sending resume")
		}
		if err := s.resume(); err != nil {
			return err
		}
		return nil
	}

	return &s
}

func (s *StmDevice) open() error {
	s.ref.Add()
	if s.ref.Count() > 1 {
		return nil
	}

	// Protected Section
	s.mu.Lock()
	var err error
	s.device, err = s.openDevice()
	if err != nil {
		s.mu.Unlock()
		// Don't hold a ref if the open failed (so that it can be retried later)
		s.ref.Remove()
		return err
	}

	if s.stopScan != nil {
		// shouldn't get here... but just in case...
		fmt.Println("stm received a request to start but it seems to already be started...")
		s.mu.Unlock()
		return nil
	}

	s.stopScan = make(chan struct{})
	s.mu.Unlock()
	// End Protected Section

	go s.readlines(s.device, s.stopScan)

	// 3 seconds should be more than enough time for the module to boot.
	if err := s.ready(time.Duration(3 * time.Second)); err != nil {
		// Don't hold a ref if the open failed (so that it can be retried later)
		s.ref.Remove()
		return fmt.Errorf("stm device is not ready")
	}

	return nil
}

func (s *StmDevice) close() error {
	s.ref.Remove()

	if s.ref.Count() >= 1 {
		return nil
	}

	s.mu.Lock()
	defer s.mu.Unlock()
	if s.stopScan != nil {
		close(s.stopScan)
		s.stopScan = nil
	}

	if err := s.device.Close(); err != nil {
		return fmt.Errorf("stm failed to close device: %w", err)
	}
	s.device = nil

	return nil
}

func NewStmGnss(path string, debug bool) *StmDevice {
	s := StmDevice{
		bus:      pubsub.New(pubsub.DefaultBufferSize),
		debug:    debug,
		path:     path,
		stopScan: nil,
	}

	s.openDevice = func() (io.ReadWriteCloser, error) {
		// Using syscall.Open will open the file in non-pollable mode, which
		// results in a significant reduction in CPU usage on ARM64 systems,
		// and no noticeable impact on x86_64. We don't need to poll the file
		// since it's just a constant stream of new data from the kernel's GNSS
		// subsystem
		fd, err := syscall.Open(s.path, os.O_RDWR, 0666)
		if err != nil {
			return nil, fmt.Errorf("stm: failed opening %q: %w", path, err)
		}
		return os.NewFile(uintptr(fd), s.path), nil
	}

	s.ready = func(t time.Duration) error {
		// device sends this message when it has booted
		resp := nmea.New("GPTXT", []string{"DEFAULT LIV CONFIGURATION"})

		sub, err := s.bus.Subscribe(internalTopic)
		if err != nil {
			return fmt.Errorf("stm unable to subscribe to internal topics: %w", err)
		}
		defer func() {
			if err := sub.Unsubscribe(); err != nil {
				fmt.Println("stm unable to unsubscribe to internal topics: ", err)
			}
		}()

		for {
			select {
			case line := <-sub.Messages:
				if bytes.Contains(line, resp) {
					return nil
				}
			case <-time.After(t):
				return fmt.Errorf("gnss/StmDevice.ready: timed out waiting for device")
			}
		}
	}

	return &s
}

func (s *StmDevice) readlines(device io.Reader, done <-chan struct{}) {
	// do the actual scan/read in a new goroutine, so that it's not
	// blocking for the parent goroutine
	lines := func() <-chan []byte {
		out := make(chan []byte)
		go func() {
			defer close(out)
			scanner := bufio.NewScanner(device)
			for scanner.Scan() {
				// Note: this forces a new allocation of the line, so it's not race-y
				data := append([]byte{}, scanner.Bytes()...)
				select {
				case out <- append(data, 0x0D, 0x0A):
				case <-done:
					return
				}
			}
			if err := scanner.Err(); err != nil {
				// meh
				return
			}
		}()
		return out
	}()

	for {
		select {
		case <-done:
			return
		case line, ok := <-lines:
			if !ok {
				// channel closed, no longer scanning
				return
			}

			if len(line) == 0 {
				continue
			}

			if err := s.bus.Publish(internalTopic, line); err != nil {
				fmt.Println("stm readlines unable to publish data: ", err)
			}
		}
	}
}

func (s *StmDevice) Start(send func(data []byte), write <-chan []byte, stop <-chan struct{}) error {
	if err := s.open(); err != nil {
		return fmt.Errorf("stm unable to open device: %w", err)
	}

	sub, err := s.bus.Subscribe(internalTopic)
	if err != nil {
		return fmt.Errorf("stm unable to start: %w", err)
	}

	go func() {
		defer func() {
			if err := s.close(); err != nil {
				fmt.Println("stm unable close device:", err)
			}
			if err := sub.Unsubscribe(); err != nil {
				fmt.Println("stm unable to unsubscribe to internal topics: ", err)
			}
		}()

		for {
			select {
			case <-stop:
				return
			case line := <-sub.Messages:
				send(line)
			case msg := <-write:
				if _, err := s.write(msg, []byte{}); err != nil {
					fmt.Println("stm message received for forwarding:", string(msg))
					fmt.Println("stm unable to forward received message to device:", err)
				}
			}
		}
	}()

	return nil
}

func (s *StmDevice) Save(dir string) (err error) {
	if err := s.open(); err != nil {
		return fmt.Errorf("stm unable to open device: %w", err)
	}
	defer func() {
		cerr := s.close()
		if err == nil {
			err = cerr
		}
	}()

	err = os.MkdirAll(dir, 0750)
	if err != nil {
		return
	}

	err = s.saveEphemeris(filepath.Join(dir, "ephemeris.txt"))
	if err != nil {
		return
	}

	err = s.saveAlamanac(filepath.Join(dir, "almanac.txt"))
	if err != nil {
		return
	}

	return
}

func (s *StmDevice) Load(dir string) (err error) {
	if err := s.open(); err != nil {
		return fmt.Errorf("stm unable to open device: %w", err)
	}
	defer func() {
		cerr := s.close()
		if err == nil {
			err = cerr
		}
	}()

	err = s.loadEphemeris(filepath.Join(dir, "ephemeris.txt"))
	if err != nil {
		return
	}

	err = s.loadAlmanac(filepath.Join(dir, "almanac.txt"))
	if err != nil {
		return
	}

	return
}

func (s *StmDevice) saveEphemeris(path string) (err error) {
	fmt.Printf("Storing ephemerides to: %q\n", path)

	if err = s.pause(); err != nil {
		return
	}
	defer func() {
		cerr := s.resume()
		if err == nil {
			err = cerr
		}
	}()

	out, err := s.write(nmea.New("PSTMDUMPEPHEMS", []string{}), []byte("DUMPEPHEMS"))
	if err != nil {
		return fmt.Errorf("gnss/Stm.saveEphemeris: %w", err)
	}

	fd, err := os.Create(filepath.Clean(path))
	if err != nil {
		err = fmt.Errorf("gnss/Stm.Save: error saving to file %q: %w", path, err)
		return
	}

	defer func() {
		cerr := fd.Close()
		if err == nil {
			err = cerr
		}
	}()

	for _, l := range out {
		if strings.HasPrefix(l, "$PSTMEPHEM,") {
			fmt.Fprintf(fd, "%s", l)
		}
	}
	return
}

func (s *StmDevice) saveAlamanac(path string) (err error) {
	fmt.Printf("Storing almanac to: %q\n", path)

	if err = s.pause(); err != nil {
		return
	}
	defer func() {
		cerr := s.resume()
		if err == nil {
			err = cerr
		}
	}()

	out, err := s.write(nmea.New("PSTMDUMPALMANAC", []string{}), []byte("DUMPALMANAC"))
	if err != nil {
		return fmt.Errorf("gnss/Stm.saveAlmanac: %w", err)
	}

	fd, err := os.Create(filepath.Clean(path))
	if err != nil {
		return fmt.Errorf("gnss/Stm.saveAlamanac: error saving to file %q: %w", path, err)
	}
	defer func() {
		cerr := fd.Close()
		if err == nil {
			err = cerr
		}
	}()

	for _, l := range out {
		if strings.HasPrefix(l, "$PSTMALMANAC,") {
			fmt.Fprintf(fd, "%s", l)
		}
	}
	return
}

// Send the given command to the module. It attempts to get confirmation by
// comparing ackResponse to the data it receives from the module (if
// ackResponse is not empty). An error is returned if ackResponse is not empty
// and it fails to find a match before it times out.
func (s *StmDevice) write(cmd []byte, ackResponse []byte) (out []string, err error) {
	sub, err := s.bus.Subscribe(internalTopic)
	if err != nil {
		return out, fmt.Errorf("stm unable to subscribe to internal topic: %w", err)
	}
	defer func() {
		if err := sub.Unsubscribe(); err != nil {
			fmt.Println("stm unable to unsubscribe to internal topics: ", err)
		}
	}()

	cmd = bytes.TrimSpace(cmd)
	if s.debug {
		fmt.Println("write: ", string(cmd))
	}
	// add crlf
	_, err = s.device.Write(append(cmd, 0x0D, 0x0A))
	if err != nil {
		err = fmt.Errorf("write: %w", err)
		return
	}

	if err != nil {
		err = fmt.Errorf("write: %w", err)
		return
	}

	if len(ackResponse) == 0 {
		return
	}

	// Note: timeout has to be high enough that some commands (e.g. reading
	// ephemeris and almanac) don't time out... Hard-coding this is somewhat
	// hacky... but it's unlikely(?) that a real comand would take more than 30
	// seconds to finish
	timeout := time.After(30 * time.Second)

timeout_loop:
	for {
		select {
		case line := <-sub.Messages:
			if s.debug {
				fmt.Printf("read: %s", line)
			}
			// Command it echo'd back when it is complete.
			if bytes.Contains(line, ackResponse) {
				break timeout_loop
			}
			out = append(out, string(line))
		case <-timeout:
			return out, fmt.Errorf("write: timed out waiting for device")
		}
	}

	return
}

func (s *StmDevice) pause() (err error) {
	_, err = s.write(nmea.New("PSTMGPSSUSPEND", []string{}), nil)
	if err != nil {
		return fmt.Errorf("gnss/Stm.pause: %w", err)
	}

	return
}

func (s *StmDevice) resume() (err error) {
	_, err = s.write(nmea.New("PSTMGPSRESTART", []string{}), nil)
	if err != nil {
		return fmt.Errorf("gnss/Stm.pause: %w", err)
	}

	return
}

func (s *StmDevice) loadEphemeris(path string) (err error) {
	fd, err := os.Open(filepath.Clean(path))
	if err != nil {
		err = fmt.Errorf("gnss/Stm.loadEphemeris: %w", err)
		return
	}
	defer func() {
		cerr := fd.Close()
		if err == nil {
			err = cerr
		}
	}()

	if err = s.pause(); err != nil {
		return
	}
	defer func() {
		cerr := s.resume()
		if err == nil {
			err = cerr
		}
	}()

	var lines [][]byte
	scanner := bufio.NewScanner(fd)
	for scanner.Scan() {
		line := bytes.TrimSpace(scanner.Bytes())
		lines = append(lines, append([]byte(nil), line...))
	}

	for _, line := range lines {
		_, err = s.write(line, []byte("EPHEMOK"))
		if err != nil {
			return fmt.Errorf("gnss/Stm.loadEphemeris: %w", err)
		}
	}

	return
}

func (s *StmDevice) loadAlmanac(path string) (err error) {
	fd, err := os.Open(filepath.Clean(path))
	if err != nil {
		err = fmt.Errorf("gnss/Stm.loadAlmanac: %w", err)
		return
	}
	defer func() {
		cerr := fd.Close()
		if err == nil {
			err = cerr
		}
	}()

	if err = s.pause(); err != nil {
		return
	}
	defer func() {
		cerr := s.resume()
		if err == nil {
			err = cerr
		}
	}()

	var lines [][]byte

	scanner := bufio.NewScanner(fd)
	for scanner.Scan() {
		line := bytes.TrimSpace(scanner.Bytes())
		lines = append(lines, append([]byte(nil), line...))
	}

	for _, line := range lines {
		_, err = s.write(line, []byte("ALMANACOK"))
		if err != nil {
			return fmt.Errorf("gnss/Stm.loadAlmanac: %w", err)
		}
	}

	return
}
