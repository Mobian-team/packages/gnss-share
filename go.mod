module gitlab.com/postmarketOS/gnss-share

go 1.15

require (
	github.com/google/uuid v1.3.0
	github.com/pelletier/go-toml v1.9.4
	github.com/tarm/serial v0.0.0-20180830185346-98f6abe2eb07
)

require golang.org/x/sys v0.0.0-20211116061358-0a5406a5449c // indirect
